import { Component, OnInit, Input } from '@angular/core';

import { Product } from 'src/app/interfaces/product';
import { ShoppingCartService } from 'src/app/services/shopping-cart.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
})
export class ProductComponent implements OnInit {
  @Input() data: Product;
  productAdded = false;

  constructor(private shoppingCartService: ShoppingCartService) {}

  ngOnInit(): void {}

  onAddProduct(product: Product) {
    this.productAdded = true;
    this.shoppingCartService.addProduct(product);
    setTimeout(() => {
      this.productAdded = false;
    }, 1000);
  }
}
