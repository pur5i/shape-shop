import {
  async,
  ComponentFixture,
  TestBed,
  fakeAsync,
  tick,
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { ProductComponent } from './product.component';
import { ShoppingCartService } from 'src/app/services/shopping-cart.service';

const mockProduct = {
  id: 10,
  name: 'Cylinder',
  description: 'A cylindrical chape',
  price: 599,
};

describe('ProductComponent', () => {
  let component: ProductComponent;
  let fixture: ComponentFixture<ProductComponent>;
  let shoppingCartService: ShoppingCartService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProductComponent],
    }).compileComponents();
    shoppingCartService = TestBed.inject(ShoppingCartService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductComponent);
    component = fixture.componentInstance;
    component.data = mockProduct;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add product', fakeAsync(() => {
    spyOn(shoppingCartService, 'addProduct');
    const btn = fixture.debugElement.query(By.css('button.product__btn'));
    btn.nativeElement.click();
    expect(shoppingCartService.addProduct).toHaveBeenCalled();
    expect(component.productAdded).toBeTrue();
    tick(1000);
    expect(component.productAdded).toBeFalse();
  }));
});
