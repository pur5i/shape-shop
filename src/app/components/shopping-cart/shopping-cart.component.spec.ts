import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { of } from 'rxjs';

import { ShoppingCartComponent } from './shopping-cart.component';
import { ShoppingCartService } from 'src/app/services/shopping-cart.service';

const mockProducts = [
  {
    description: 'Lorem ipsum circle',
    id: 1,
    name: 'Mock Circle',
    price: 999,
    amount: 1,
  },
  {
    description: 'Lorem ipsum rectangle',
    id: 2,
    name: 'Mock Rectangle',
    price: 899,
    amount: 1,
  },
  {
    description: 'Lorem ipsum triangle',
    id: 3,
    name: 'Mock Triangle',
    price: 1009,
    amount: 1,
  },
];

describe('ShoppingCartComponent', () => {
  let component: ShoppingCartComponent;
  let fixture: ComponentFixture<ShoppingCartComponent>;
  let shoppingCartService: ShoppingCartService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ShoppingCartComponent],
    }).compileComponents();
    shoppingCartService = TestBed.inject(ShoppingCartService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShoppingCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should fetch product and manage and calculate cart on init', (done) => {
    spyOn(shoppingCartService, 'getProduct').and.returnValue(
      of(mockProducts[0])
    );
    spyOn(component as any, 'manageCartContent');
    spyOn(component as any, 'calcItemsInCart');
    component.ngOnInit();

    expect(shoppingCartService.getProduct).toHaveBeenCalled();

    shoppingCartService.getProduct().subscribe((product) => {
      expect(component['manageCartContent']).toHaveBeenCalledWith(product);
      expect(component['calcItemsInCart']).toHaveBeenCalled();
      done();
    });
  });

  it('should increment product amount when adding existing product', () => {
    component.products = [...mockProducts];
    component['manageCartContent'](mockProducts[0]);
    expect(component.products[0]).toEqual({
      description: 'Lorem ipsum circle',
      id: 1,
      name: 'Mock Circle',
      price: 999,
      amount: 2,
    });
  });

  it('should add new product if none exists', () => {
    const mock = {
      description: 'Lorem ipsum mock',
      id: 10,
      name: 'Mock Product',
      price: 1299,
    };
    component.products = [...mockProducts];
    component['manageCartContent'](mock);
    expect(component.products[3]).toEqual({ ...mock, amount: 1 });
  });

  it('should empty the cart and reset cart state', () => {
    component.products = [...mockProducts];
    component.emptyCart();
    expect(component.products).toEqual([]);
    expect(component.productCount).toEqual(0);
    expect(component.cartVisible).toBeFalse();
    expect(component.totalPrice).toEqual(0);
  });

  it('should remove a product from the cart', () => {
    component.products = [...mockProducts];
    const expected = component.products.slice(1);
    component.removeProduct(component.products[0].id);
    expect(component.products).toEqual(expected);
  });
});
