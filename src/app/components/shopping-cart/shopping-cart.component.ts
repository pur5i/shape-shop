import { Component, OnInit, OnDestroy } from '@angular/core';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { Product } from 'src/app/interfaces/product';
import { ShoppingCartService } from 'src/app/services/shopping-cart.service';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss'],
})
export class ShoppingCartComponent implements OnInit, OnDestroy {
  products: Product[] = [];
  productCount = 0;
  totalPrice = 0;
  ngDestroyed$ = new Subject();
  cartVisible = false;

  constructor(private shoppingCartService: ShoppingCartService) {}

  ngOnInit() {
    this.shoppingCartService
      .getProduct()
      .pipe(takeUntil(this.ngDestroyed$))
      .subscribe((product: Product) => {
        this.manageCartContent(product);
        this.calcItemsInCart();
      });
  }

  ngOnDestroy() {
    this.ngDestroyed$.next();
  }

  onDecreaseAmount(product: Product) {
    this.products.forEach((item) => {
      if (item.id === product.id && item.amount !== 0) {
        item.amount--;

        if (item.amount === 0) {
          this.removeProduct(item.id);
        }
      }
    });
    this.calcItemsInCart();
  }

  onIncreaseAmount(product: Product) {
    this.products.forEach((item) => {
      if (item.id === product.id) {
        item.amount++;
      }
    });
    this.calcItemsInCart();
  }

  removeProduct(id: number) {
    this.products = this.products.filter((p) => p.id !== id);
    this.calcItemsInCart();
  }

  emptyCart() {
    this.products = [];
    this.calcItemsInCart();
  }

  private manageCartContent(product: Product) {
    if (!this.productExists(product)) {
      this.products.push({ ...product, amount: 1 });
    } else {
      this.products.forEach((item) => {
        if (item.id === product.id) {
          item.amount++;
        }
      });
    }
  }

  private productExists(product: Product): boolean {
    return this.products.some((item) => item.id === product.id);
  }

  private calcItemsInCart() {
    this.productCount = this.products.reduce((acc, obj) => acc + obj.amount, 0);
    this.totalPrice = this.products.reduce(
      (acc, obj) => acc + obj.amount * obj.price,
      0
    );

    if (this.productCount === 0) {
      this.cartVisible = false;
    }
  }
}
