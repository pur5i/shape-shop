import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { map, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

import { Product } from '../interfaces/product';

@Injectable({
  providedIn: 'root',
})
export class ProductsService {
  constructor(private http: HttpClient) {}

  getProducts(): Observable<Product[]> {
    return this.http
      .get('https://shape-shop-66785.firebaseio.com/products.json')
      .pipe(
        map((products: Product[]): Product[] => products),
        catchError((error) => throwError(error))
      );
  }
}
