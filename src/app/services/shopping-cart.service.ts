import { Injectable } from '@angular/core';

import { Subject, Observable } from 'rxjs';

import { Product } from '../interfaces/product';

@Injectable({
  providedIn: 'root',
})
export class ShoppingCartService {
  private subject = new Subject<Product>();

  constructor() {}

  addProduct(product: Product) {
    this.subject.next(product);
  }

  getProduct(): Observable<Product> {
    return this.subject.asObservable();
  }
}
